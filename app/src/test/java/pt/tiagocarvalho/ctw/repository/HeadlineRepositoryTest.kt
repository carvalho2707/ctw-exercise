package pt.tiagocarvalho.ctw.repository

import com.google.gson.Gson
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import pt.tiagocarvalho.ctw.api.HeadlineService
import pt.tiagocarvalho.ctw.api.model.HeadlineInfoResponse
import pt.tiagocarvalho.ctw.api.model.HeadlineResponse
import pt.tiagocarvalho.ctw.data.dao.HeadlineDao
import pt.tiagocarvalho.ctw.data.model.HeadlineEntity
import pt.tiagocarvalho.ctw.model.Resource
import java.io.IOException
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class HeadlineRepositoryTest {

    companion object {
        private const val SOURCES = "bbc-news"
        private const val SUCCESS_STATUS = "ok"
    }

    @Mock
    lateinit var headlineService: HeadlineService

    @Mock
    lateinit var headlineDao: HeadlineDao

    private val gson = Gson()

    private lateinit var headlineRepository: HeadlineRepository

    @Before
    fun setup() {
        headlineRepository = HeadlineRepository(headlineService, headlineDao, gson)
    }

    @Test
    fun whenGetTopHeadlinesAndSuccess_shouldReturnEntityList() {
        val entities = generateEntityList()

        val article = HeadlineInfoResponse(
            "https://www.bbc.com/news/election-us-2020-54800337",
            "US election results 2020: Why don't we know who has won?",
            Date(1604515046000),
            "https://ichef.bbci.co.uk/news/1024/branded_news/3392/production/_115220231_biden_trump_supporters_getty_reuters.jpg",
            "ou might have reasonably expected to have some kind of clue about the results of the US presidential election by now.\\r\\nWe don't know, because not enough votes have been counted yet for either Donald… [+3870 chars]",
            "The fight for the presidency is down to a few crucial states but it could be days before votes get counted."
        )
        val successHeadline = HeadlineResponse(SUCCESS_STATUS, 1, listOf(article))

        `when`(headlineService.getTopHeadlines(SOURCES)).thenReturn(Single.just(successHeadline))
        `when`(headlineDao.deleteAll()).thenReturn(Completable.complete())
        `when`(headlineDao.insertAll(generateEntityList())).thenReturn(Completable.complete())
        `when`(headlineDao.getAll()).thenReturn(Single.just(entities))

        val testObserver = headlineRepository.getTopHeadlines().test()

        verify(headlineService).getTopHeadlines(SOURCES)
        verify(headlineDao).deleteAll()
        verify(headlineDao).insertAll(generateEntityList())
        verify(headlineDao, times(1)).getAll()

        val expectedResult = Resource.success(entities)

        testObserver.assertNoErrors()
        testObserver.assertValue(expectedResult)
    }

    @Test
    fun whenGetTopHeadlinesWithNulls_shouldReturnEntityList() {
        val entities = generateEntityListWithNulls()

        val article = HeadlineInfoResponse(
            "https://www.bbc.com/news/election-us-2020-54800337",
            "US election results 2020: Why don't we know who has won?",
            Date(1604515046000),
            null,
            null,
            null
        )
        val successHeadline = HeadlineResponse(SUCCESS_STATUS, 1, listOf(article))

        `when`(headlineService.getTopHeadlines(SOURCES)).thenReturn(Single.just(successHeadline))
        `when`(headlineDao.deleteAll()).thenReturn(Completable.complete())
        `when`(headlineDao.insertAll(generateEntityListWithNulls())).thenReturn(Completable.complete())//TODO ARGUMENT MATCHER
        `when`(headlineDao.getAll()).thenReturn(Single.just(entities))

        val testObserver = headlineRepository.getTopHeadlines().test()

        verify(headlineService).getTopHeadlines(SOURCES)
        verify(headlineDao).deleteAll()
        verify(headlineDao).insertAll(generateEntityListWithNulls())
        verify(headlineDao, times(1)).getAll()

        val expectedResult = Resource.success(entities)
        testObserver.assertNoErrors()
        testObserver.assertValue(expectedResult)
    }

    @Test
    fun whenError_shouldResumeFromDatabase() {
        val entities = generateEntityListWithNulls()

        `when`(headlineService.getTopHeadlines(SOURCES)).thenReturn(
            Single.error(IOException("Something went wrong"))
        )
        `when`(headlineDao.deleteAll()).thenReturn(Completable.complete())
        `when`(headlineDao.insertAll(generateEntityListWithNulls())).thenReturn(Completable.complete())//TODO ARGUMENT MATCHER
        `when`(headlineDao.getAll()).thenReturn(Single.just(entities))

        val testObserver = headlineRepository.getTopHeadlines().test()

        verify(headlineService).getTopHeadlines(SOURCES)
        verify(headlineDao, times(0)).deleteAll()
        verify(headlineDao, times(0)).insertAll(generateEntityListWithNulls())
        verify(headlineDao, times(1)).getAll()

        val expectedResult = Resource.error("Something went wrong", entities)
        testObserver.assertNoErrors()
        testObserver.assertValue(expectedResult)
    }

    @Test
    fun whenGetHeadlineAndSuccess_shouldReturnEntity() {
        val expectedResult = generateEntityList().first()
        val url = "https://www.bbc.com/news/election-us-2020-54800337"

        `when`(headlineDao.getById(url)).thenReturn(Single.just(expectedResult))

        val testObserver = headlineRepository.getHeadline(url).test()

        verifyNoInteractions(headlineService)
        verify(headlineDao).getById(url)
        verifyNoMoreInteractions(headlineDao)

        testObserver.assertNoErrors()
        testObserver.assertValue(expectedResult)
    }

    private fun generateEntityList(): List<HeadlineEntity> {
        val article = HeadlineEntity(
            "https://www.bbc.com/news/election-us-2020-54800337",
            "US election results 2020: Why don't we know who has won?",
            Date(1604515046000),
            "https://ichef.bbci.co.uk/news/1024/branded_news/3392/production/_115220231_biden_trump_supporters_getty_reuters.jpg",
            "ou might have reasonably expected to have some kind of clue about the results of the US presidential election by now.\\r\\nWe don't know, because not enough votes have been counted yet for either Donald… [+3870 chars]",
            "The fight for the presidency is down to a few crucial states but it could be days before votes get counted."
        )
        return listOf(article)
    }

    private fun generateEntityListWithNulls(): List<HeadlineEntity> {
        val article = HeadlineEntity(
            "https://www.bbc.com/news/election-us-2020-54800337",
            "US election results 2020: Why don't we know who has won?",
            Date(1604515046000),
            null,
            null,
            null
        )
        return listOf(article)
    }
}