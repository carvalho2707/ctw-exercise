package pt.tiagocarvalho.ctw.api

import io.reactivex.rxjava3.observers.TestObserver
import io.reactivex.rxjava3.schedulers.TestScheduler
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import pt.tiagocarvalho.ctw.api.model.HeadlineResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class HeadlineServiceTest {

    companion object {
        private const val SOURCES = "bbc-news"
    }

    private lateinit var service: HeadlineService
    private lateinit var mockWebServer: MockWebServer
    private lateinit var scheduler: TestScheduler

    @Before
    fun setup() {
        mockWebServer = MockWebServer()

        val client = OkHttpClient.Builder()
            .addInterceptor(NewsApiInterceptor())
            .build()

        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(client)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(HeadlineService::class.java)

        scheduler = TestScheduler()
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun whenGetTopHeadlines_shouldReturn() {
        enqueueResponse()
        val testObserver = TestObserver<HeadlineResponse>()
        service.getTopHeadlines(SOURCES)
            .subscribe(testObserver)

        testObserver.await()
        testObserver.assertNoErrors()

        val values: List<HeadlineResponse> = testObserver.values()
        assertEquals(1, values.size)

        val headlineResponse = values[0]
        assertEquals("ok", headlineResponse.status)
        assertEquals(2, headlineResponse.totalResults)
        assertEquals(2, headlineResponse.articles?.size)
    }

    private fun enqueueResponse() {
        val inputStream = javaClass.classLoader!!
            .getResourceAsStream("api-response/headlines.json")
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        mockWebServer.enqueue(
            mockResponse
                .setBody(source.readString(Charsets.UTF_8))
        )
    }
}