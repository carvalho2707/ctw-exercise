package pt.tiagocarvalho.ctw.di

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import pt.tiagocarvalho.ctw.api.HeadlineService
import pt.tiagocarvalho.ctw.data.dao.HeadlineDao
import pt.tiagocarvalho.ctw.repository.HeadlineRepository
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideHeadlineRepository(
        headlineService: HeadlineService,
        headlineDao: HeadlineDao,
        gson: Gson
    ) = HeadlineRepository(headlineService, headlineDao, gson)
}
