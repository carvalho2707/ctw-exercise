package pt.tiagocarvalho.ctw.di

import android.content.Context
import android.content.pm.PackageManager
import androidx.biometric.BiometricManager
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class AppModule {

    @Singleton
    @Provides
    fun providesBiometricManager(@ApplicationContext appContext: Context) =
        BiometricManager.from(appContext)

    @Singleton
    @Provides
    fun providePackageManager(@ApplicationContext appContext: Context): PackageManager =
        appContext.packageManager

    @Singleton
    @Provides
    fun provideGson(): Gson = Gson()
}
