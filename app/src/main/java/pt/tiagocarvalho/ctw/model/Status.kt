package pt.tiagocarvalho.ctw.model

enum class Status {
    LOADING, SUCCESS, ERROR;
}
