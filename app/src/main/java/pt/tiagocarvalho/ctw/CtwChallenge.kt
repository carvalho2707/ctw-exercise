package pt.tiagocarvalho.ctw

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CtwChallenge : Application()
