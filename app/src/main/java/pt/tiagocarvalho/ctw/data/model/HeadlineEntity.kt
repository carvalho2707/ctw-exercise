package pt.tiagocarvalho.ctw.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "headline")
data class HeadlineEntity(
    @PrimaryKey val url: String,
    val title: String,
    val publishedAt: Date,
    val urlToImage: String?,
    val content: String?,
    val description: String?
)
