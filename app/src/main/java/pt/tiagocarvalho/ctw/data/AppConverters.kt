package pt.tiagocarvalho.ctw.data

import androidx.room.TypeConverter
import java.util.Date

object AppConverters {

    @TypeConverter
    @JvmStatic
    fun toDate(dateLong: Long?): Date? = if (dateLong == null) null else Date(dateLong)

    @TypeConverter
    @JvmStatic
    fun fromDate(date: Date?): Long? = date?.time
}
