package pt.tiagocarvalho.ctw.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import pt.tiagocarvalho.ctw.data.model.HeadlineEntity

@Dao
interface HeadlineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg: List<HeadlineEntity>): Completable

    @Query("DELETE from headline")
    fun deleteAll(): Completable

    @Query("SELECT * FROM headline ORDER BY publishedAt DESC")
    fun getAll(): Single<List<HeadlineEntity>>

    @Query("SELECT * FROM headline WHERE url = :id")
    fun getById(id: String): Single<HeadlineEntity>
}
