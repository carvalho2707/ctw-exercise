package pt.tiagocarvalho.ctw.api

import io.reactivex.rxjava3.core.Single
import pt.tiagocarvalho.ctw.api.model.HeadlineResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface HeadlineService {

    @GET("v2/top-headlines")
    fun getTopHeadlines(@Query("sources") source: String): Single<HeadlineResponse>
}
