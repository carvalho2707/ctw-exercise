package pt.tiagocarvalho.ctw.api

import okhttp3.Interceptor
import okhttp3.Response
import pt.tiagocarvalho.ctw.BuildConfig

class NewsApiInterceptor : Interceptor {

    companion object {
        const val API_KEY_HEADER = "X-Api-Key"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        with(chain.request()) {
            val requestWithAuth = newBuilder()
                .header(API_KEY_HEADER, BuildConfig.NEWS_API_KEY)
                .build()
            return chain.proceed(requestWithAuth)
        }
    }
}
