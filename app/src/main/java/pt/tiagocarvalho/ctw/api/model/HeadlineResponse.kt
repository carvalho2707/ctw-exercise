package pt.tiagocarvalho.ctw.api.model

import java.util.*

data class HeadlineResponse(
    val status: String,
    val totalResults: Int?,
    val articles: List<HeadlineInfoResponse>?
)

data class HeadlineInfoResponse(
    val url: String,
    val title: String,
    val publishedAt: Date,
    val urlToImage: String?,
    val content: String?,
    val description: String?
)

data class ErrorResponse(
    val status: String,
    val code: String,
    val message: String
)
