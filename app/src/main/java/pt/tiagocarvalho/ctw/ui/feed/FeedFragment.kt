package pt.tiagocarvalho.ctw.ui.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.ctw.R
import pt.tiagocarvalho.ctw.databinding.FragmentFeedBinding
import pt.tiagocarvalho.ctw.model.Status
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class FeedFragment : Fragment() {

    private lateinit var binding: FragmentFeedBinding
    private lateinit var feedAdapter: FeedAdapter

    private val viewModel: FeedViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFeedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedElementReturnTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.item_photo_enter)
        postponeEnterTransition(500L, TimeUnit.MILLISECONDS)

        feedAdapter = FeedAdapter(this::navigateToDetails)

        binding.rvFeed.adapter = feedAdapter

        viewModel.model.observe(viewLifecycleOwner) {
            val model = it ?: return@observe
            updateUi(model)
        }

        viewModel.loadFeed()
    }

    private fun navigateToDetails(url: String, photo: ImageView) {
        val extras = FragmentNavigatorExtras(photo to photo.transitionName)
        val direction =
            FeedFragmentDirections.toDetails(url)
        findNavController().navigate(direction, extras)
    }

    private fun updateUi(model: FeedModel) {
        when {
            model.status == Status.ERROR -> {
                if (model.items != null) {
                    binding.rvFeed.visibility = View.VISIBLE
                    binding.message.visibility = View.GONE
                    feedAdapter.submitList(model.items)
                } else {
                    binding.rvFeed.visibility = View.GONE
                    binding.message.apply {
                        visibility = View.VISIBLE
                        setText(R.string.feed_error)
                    }
                }
            }
            model.status == Status.LOADING -> {
                binding.rvFeed.visibility = View.GONE
                binding.message.apply {
                    visibility = View.VISIBLE
                    setText(R.string.feed_loading)
                }
            }
            else -> {
                if (model.items.isNullOrEmpty()) {
                    binding.rvFeed.visibility = View.GONE
                    binding.message.apply {
                        visibility = View.VISIBLE
                        setText(R.string.feed_no_results)
                    }
                } else {
                    binding.rvFeed.visibility = View.VISIBLE
                    binding.message.visibility = View.GONE
                    feedAdapter.submitList(model.items)
                }
            }
        }
    }
}
