package pt.tiagocarvalho.ctw.ui.splash

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.ctw.R
import pt.tiagocarvalho.ctw.databinding.ActivitySplashBinding
import pt.tiagocarvalho.ctw.ui.MainActivity

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding
    private val viewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.action.observe(this) {
            val action = it ?: return@observe
            onActionReceived(action)
        }

        viewModel.checkFingerprint()
    }

    private fun onActionReceived(action: SplashAction) {
        when (action) {
            SplashAction.FEED -> startActivity(MainActivity().getStartIntent(this))
            SplashAction.FINGERPRINT -> with(createBiometricPrompt()) {
                showPrompt(this)
            }
            else -> finish()
        }
    }

    private fun createBiometricPrompt(): BiometricPrompt {
        val executor = ContextCompat.getMainExecutor(this)
        return BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    navigateToFeed()
                }
            })
    }

    private fun navigateToFeed() = startActivity(MainActivity().getStartIntent(this@SplashActivity))

    private fun showPrompt(biometricPrompt: BiometricPrompt) {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.splash_fingerprint_authentication_required_title))
            .setSubtitle(getString(R.string.splash_fingerprint_authentication_required_body))
            .setDeviceCredentialAllowed(true)
            .build()
        biometricPrompt.authenticate(promptInfo)
    }
}
