package pt.tiagocarvalho.ctw.ui.feed

import io.reactivex.rxjava3.core.Single
import pt.tiagocarvalho.ctw.data.model.HeadlineEntity
import pt.tiagocarvalho.ctw.model.Resource
import pt.tiagocarvalho.ctw.model.Status
import pt.tiagocarvalho.ctw.repository.HeadlineRepository
import javax.inject.Inject

open class FeedUseCase @Inject constructor(private val headlineRepository: HeadlineRepository) {

    fun getTopHeadlines(): Single<FeedModel> {
        return headlineRepository.getTopHeadlines()
            .map { mapToFeedModel(it) }
            .onErrorReturn { mapToFeedItem(it) }
    }

    private fun mapToFeedItem(throwable: Throwable): FeedModel {
        return FeedModel(status = Status.ERROR, errorMessage = throwable.message)
    }

    private fun mapToFeedModel(resource: Resource<List<HeadlineEntity>>): FeedModel {
        when {
            resource.status == Status.SUCCESS && resource.data != null -> {
                val items = mapToFeedItem(resource.data)
                return FeedModel(
                    status = Status.SUCCESS,
                    items = items
                )
            }
            resource.status == Status.ERROR && resource.data != null -> {
                val items = mapToFeedItem(resource.data)
                return FeedModel(
                    status = Status.ERROR,
                    items = items,
                    errorMessage = resource.message
                )
            }
            else -> {
                return FeedModel(
                    status = Status.ERROR,
                    errorMessage = resource.message
                )
            }
        }
    }

    private fun mapToFeedItem(entities: List<HeadlineEntity>): List<FeedItem> {
        return entities.map {
            FeedItem(
                it.url,
                it.title,
                it.urlToImage
            )
        }
    }
}
