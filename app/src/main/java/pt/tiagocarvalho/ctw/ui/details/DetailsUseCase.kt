package pt.tiagocarvalho.ctw.ui.details

import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import pt.tiagocarvalho.ctw.data.model.HeadlineEntity
import pt.tiagocarvalho.ctw.repository.HeadlineRepository

class DetailsUseCase @Inject constructor(private val feedRepository: HeadlineRepository) {

    fun getHeadline(url: String): Single<DetailsModel> {
        return feedRepository.getHeadline(url)
            .map { mapToDetailsModel(it) }
    }

    private fun mapToDetailsModel(entity: HeadlineEntity): DetailsModel {
        return DetailsModel(
            entity.urlToImage,
            entity.title,
            entity.description,
            entity.content
        )
    }
}
