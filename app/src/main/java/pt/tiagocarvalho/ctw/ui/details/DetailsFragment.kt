package pt.tiagocarvalho.ctw.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.ctw.R
import pt.tiagocarvalho.ctw.databinding.FragmentDetailsBinding
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class DetailsFragment : Fragment() {

    private lateinit var binding: FragmentDetailsBinding
    private val viewModel: DetailsViewModel by viewModels()
    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.image.transitionName = args.url

        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.item_photo_enter)
        postponeEnterTransition(500L, TimeUnit.MILLISECONDS)

        viewModel.model.observe(viewLifecycleOwner) {
            val model = it ?: return@observe
            updateUi(model)
        }

        viewModel.loadFeed(args.url)
    }

    private fun updateUi(model: DetailsModel) {
        binding.apply {
            startPostponedEnterTransition()

            Glide.with(image)
                .load(model.imageUrl)
                .placeholder(R.drawable.ic_feed_placeholder)
                .error(R.drawable.ic_feed_error)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(image)

            title.text = model.title

            if (model.description.isNullOrBlank()) {
                description.visibility = View.GONE
            } else {
                description.text = model.description
                description.visibility = View.VISIBLE
            }

            if (model.content.isNullOrBlank()) {
                contentLayout.cardLayout.visibility = View.GONE
            } else {
                contentLayout.title.text = model.content
                contentLayout.cardLayout.visibility = View.VISIBLE
            }
        }
    }
}
