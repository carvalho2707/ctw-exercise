package pt.tiagocarvalho.ctw.ui.feed

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.rxjava3.schedulers.Schedulers
import pt.tiagocarvalho.ctw.model.Status
import pt.tiagocarvalho.ctw.ui.base.BaseViewModel

class FeedViewModel @ViewModelInject constructor(private val feedUseCase: FeedUseCase) :
    BaseViewModel() {

    private val _model = MutableLiveData<FeedModel>()
    val model: LiveData<FeedModel> = _model

    fun loadFeed() {
        disposable.add(
            feedUseCase.getTopHeadlines()
                .subscribeOn(Schedulers.io())
                .doOnSuccess { _model.postValue(it) }
                .doOnSubscribe { _model.value = FeedModel(status = Status.LOADING) }
                .subscribe()
        )
    }
}
