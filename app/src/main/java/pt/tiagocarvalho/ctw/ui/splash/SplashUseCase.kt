package pt.tiagocarvalho.ctw.ui.splash

import android.content.pm.PackageManager
import android.os.Build
import androidx.biometric.BiometricManager
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SplashUseCase @Inject constructor(
    private val biometricManager: BiometricManager,
    private val packageManager: PackageManager
) {

    fun loadSplashAction(): Single<SplashAction> = Single.create {
        val compatibility = getDeviceCompatibility()
        it.onSuccess(mapToSplashAction(compatibility))
    }

    private fun mapToSplashAction(deviceCompatibility: Int): SplashAction {
        return if (deviceCompatibility == BiometricManager.BIOMETRIC_SUCCESS) SplashAction.FINGERPRINT
        else SplashAction.FEED
    }

    private fun getDeviceCompatibility(): Int {
        val biometricCompatibility = biometricManager.canAuthenticate()
        // User may have biometrics but no fingerprint (e.g face recognition)
        if (hasBiometrics(biometricCompatibility) && !hasFingerPrint()) {
            return BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE
        }
        return biometricCompatibility
    }

    private fun hasBiometrics(biometricCompatibility: Int): Boolean =
        biometricCompatibility == BiometricManager.BIOMETRIC_SUCCESS ||
            biometricCompatibility == BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED

    private fun hasFingerPrint(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) packageManager.hasSystemFeature(
            PackageManager.FEATURE_FINGERPRINT
        ) else false
    }
}
