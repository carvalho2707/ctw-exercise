package pt.tiagocarvalho.ctw.ui.details

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.rxjava3.schedulers.Schedulers
import pt.tiagocarvalho.ctw.ui.base.BaseViewModel

class DetailsViewModel @ViewModelInject constructor(private val detailsUseCase: DetailsUseCase) :
    BaseViewModel() {

    private val _model = MutableLiveData<DetailsModel>()
    val model: LiveData<DetailsModel> = _model

    fun loadFeed(url: String) {
        disposable.add(
            detailsUseCase.getHeadline(url)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { _model.postValue(it) }
                .subscribe()
        )
    }
}
