package pt.tiagocarvalho.ctw.ui.feed

data class FeedItem(
    // since there is no id in NewsAPI we will consider url as an id and unique
    val url: String,
    val title: String,
    val imageUrl: String?
)
