package pt.tiagocarvalho.ctw.ui.feed

import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import pt.tiagocarvalho.ctw.R
import pt.tiagocarvalho.ctw.databinding.ItemFeedBinding

class FeedViewHolder(private val binding: ItemFeedBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: FeedItem, onItemClicked: (String, ImageView) -> Unit) {

        binding.apply {
            image.transitionName = item.url

            Glide.with(image)
                .load(item.imageUrl)
                .placeholder(R.drawable.ic_feed_placeholder)
                .error(R.drawable.ic_feed_error)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(image)

            title.text = item.title

            root.setOnClickListener {
                onItemClicked(item.url, image)
            }
        }
    }
}
