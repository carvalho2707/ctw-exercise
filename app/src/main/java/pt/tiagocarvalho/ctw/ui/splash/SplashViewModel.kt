package pt.tiagocarvalho.ctw.ui.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import pt.tiagocarvalho.ctw.ui.base.BaseViewModel

class SplashViewModel @ViewModelInject constructor(private val splashUseCase: SplashUseCase) :
    BaseViewModel() {

    private val _action = MutableLiveData<SplashAction>()
    val action: LiveData<SplashAction> = _action

    fun checkFingerprint() {
        disposable.add(
            splashUseCase.loadSplashAction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { _action.value = it }
                .doOnError { _action.value = SplashAction.EXIT }
                .subscribe({}, { it.printStackTrace() })
        )
    }
}
