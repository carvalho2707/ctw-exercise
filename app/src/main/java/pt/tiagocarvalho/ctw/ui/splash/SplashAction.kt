package pt.tiagocarvalho.ctw.ui.splash

enum class SplashAction {
    FINGERPRINT, FEED, EXIT
}
