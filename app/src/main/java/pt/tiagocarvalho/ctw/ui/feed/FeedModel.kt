package pt.tiagocarvalho.ctw.ui.feed

import pt.tiagocarvalho.ctw.model.Status

data class FeedModel(
    val status: Status,
    val items: List<FeedItem>? = null,
    val errorMessage: String? = null
)
