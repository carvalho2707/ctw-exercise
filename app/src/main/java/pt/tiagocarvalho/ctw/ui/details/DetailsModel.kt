package pt.tiagocarvalho.ctw.ui.details

data class DetailsModel(
    val imageUrl: String?,
    val title: String,
    val description: String?,
    val content: String?
)
