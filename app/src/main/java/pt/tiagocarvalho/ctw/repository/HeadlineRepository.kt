package pt.tiagocarvalho.ctw.repository

import com.google.gson.Gson
import io.reactivex.rxjava3.core.Single
import pt.tiagocarvalho.ctw.BuildConfig
import pt.tiagocarvalho.ctw.api.HeadlineService
import pt.tiagocarvalho.ctw.api.model.ErrorResponse
import pt.tiagocarvalho.ctw.api.model.HeadlineInfoResponse
import pt.tiagocarvalho.ctw.data.dao.HeadlineDao
import pt.tiagocarvalho.ctw.data.model.HeadlineEntity
import pt.tiagocarvalho.ctw.model.Resource
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class HeadlineRepository @Inject constructor(
    private val headlineService: HeadlineService,
    private val headlineDao: HeadlineDao,
    private val gson: Gson
) {

    fun getTopHeadlines(): Single<Resource<List<HeadlineEntity>>> {
        return headlineService.getTopHeadlines(BuildConfig.NEWS_SOURCE)
            .map { it.articles }
            .flatMap {
                headlineDao.deleteAll()
                    .andThen(headlineDao.insertAll(mapToHeadlineEntity(it!!)))
                    .andThen(headlineDao.getAll())
                    .map { headlines -> Resource.success(headlines) }
            }
            .onErrorResumeNext { throwable ->
                headlineDao.getAll()
                    .map { mapToError(it, throwable) }
            }
    }

    fun getHeadline(id: String): Single<HeadlineEntity> {
        return headlineDao.getById(id)
    }

    private fun mapToHeadlineEntity(headlines: List<HeadlineInfoResponse>): List<HeadlineEntity> {
        return headlines.map {
            HeadlineEntity(
                it.url,
                it.title,
                it.publishedAt,
                it.urlToImage,
                it.content,
                it.description
            )
        }
    }

    private fun mapToError(
        entities: List<HeadlineEntity>,
        throwable: Throwable
    ): Resource<List<HeadlineEntity>> {
        return if (throwable is HttpException) {
            val message = throwable.message()
            val errorResponse = gson.fromJson(message, ErrorResponse::class.java)
            Resource.error(errorResponse.message, entities)
        } else {
            Resource.error(throwable.message, entities)
        }
    }
}
