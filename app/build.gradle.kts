plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
    id("dagger.hilt.android.plugin")
    id("org.jlleitschuh.gradle.ktlint")
}

android {
    compileSdkVersion(30)

    defaultConfig {
        applicationId = "pt.tiagocarvalho.ctw"
        minSdkVersion(21)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        buildConfigField("String", "NEWS_API_URL", properties["NEWS_BASE_URL"] as String)
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            buildConfigField("String", "NEWS_API_KEY", properties["NEWS_API_KEY"] as String)
        }

        getByName("debug") {
            buildConfigField("String", "NEWS_API_KEY", properties["NEWS_API_KEY"] as String)
        }
    }

    flavorDimensions("sources")
    productFlavors {
        create("bbcnews") {
            setDimension("sources")
            buildConfigField("String", "NEWS_SOURCE", properties["BBC_NEWS_SOURCE"] as String)
        }
        create("abcnews") {
            setDimension("sources")
            buildConfigField("String", "NEWS_SOURCE", properties["ABC_NEWS_SOURCE"] as String)
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    // To avoid the compile error: "Cannot inline bytecode built with JVM target 1.8
    // into bytecode that is being built with JVM target 1.6"
    kotlinOptions {
        val options = this as org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions
        options.jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation("org.jetbrains.kotlin:kotlin-stdlib:${Versions.Kotlin}")

    implementation("androidx.core:core-ktx:${Versions.Core}")
    implementation("androidx.appcompat:appcompat:${Versions.AppCompat}")

    implementation("androidx.activity:activity-ktx:${Versions.Activity}")
    implementation("androidx.fragment:fragment-ktx:${Versions.Fragment}")
    implementation("androidx.constraintlayout:constraintlayout:${Versions.ConstraintLayout}")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:${Versions.Lifecycle}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.Lifecycle}")

    implementation("androidx.biometric:biometric:${Versions.Biometrics}")

    implementation("com.google.android.material:material:${Versions.Material}")

    implementation("androidx.navigation:navigation-fragment-ktx:${Versions.Navigation}")
    implementation("androidx.navigation:navigation-ui-ktx:${Versions.Navigation}")

    kapt("androidx.room:room-compiler:${Versions.Room}")
    implementation("androidx.room:room-runtime:${Versions.Room}")
    implementation("androidx.room:room-ktx:${Versions.Room}")
    implementation("androidx.room:room-rxjava3:${Versions.Room}")

    implementation("com.google.dagger:hilt-android:${Versions.Hilt}")
    implementation("androidx.hilt:hilt-lifecycle-viewmodel:${Versions.HiltViewModel}")
    kapt("com.google.dagger:hilt-android-compiler:${Versions.Hilt}")
    kapt("androidx.hilt:hilt-compiler:${Versions.HiltViewModel}")

    implementation("io.reactivex.rxjava3:rxkotlin:${Versions.RxKotlin}")
    implementation("io.reactivex.rxjava3:rxandroid:${Versions.RxAndroid}")

    kapt("com.github.bumptech.glide:compiler:${Versions.Glide}")
    implementation("com.github.bumptech.glide:glide:${Versions.Glide}")

    implementation("com.squareup.retrofit2:retrofit:${Versions.Retrofit}")
    implementation("com.squareup.retrofit2:converter-gson:${Versions.Retrofit}")
    implementation("com.squareup.retrofit2:adapter-rxjava3:${Versions.Retrofit}")
    implementation("com.squareup.okhttp3:logging-interceptor:${Versions.OkHttpLogging}")

    testImplementation("junit:junit:${Versions.JUnit}")
    testImplementation("org.mockito:mockito-core:${Versions.Mockito}")
    testImplementation("com.squareup.okhttp3:mockwebserver:${Versions.MockWebServer}")
    androidTestImplementation("androidx.test.ext:junit:${Versions.ExtJUnit}")
    androidTestImplementation("androidx.test.espresso:espresso-core:${Versions.Espresso}")
}

ktlint {
    android.set(true)
    outputColorName.set("RED")
}
